import hlt.*;

import java.util.ArrayList;

public class MyBot {

    public static void main(final String[] args) {
        final Networking networking = new Networking();
        final GameMap gameMap = networking.initialize("Tamagocchi");

        // We now have 1 full minute to analyse the initial map.
        final String initialMapIntelligence =
                "width: " + gameMap.getWidth() +
                "; height: " + gameMap.getHeight() +
                "; players: " + gameMap.getAllPlayers().size() +
                "; planets: " + gameMap.getAllPlanets().size();
        Log.log(initialMapIntelligence);

        final ArrayList<Move> moveList = new ArrayList<>();
        for (;;) {
            moveList.clear();
            networking.updateMap(gameMap);

            for (final Ship ship : gameMap.getMyPlayer().getShips().values()) {
                if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
                    continue;
                }

                for (final Planet planet : gameMap.getAllPlanets().values()) {
                    if (planet.isOwned()) {
                        continue;
                    }

                    entitiesByDistance = gameMap.nearbyEntitiesByDistance(ship);
                    nearestPlanet = null;
                    entitiesByDistance.sort();
                    for (Map.Entry<Double, Entity> entry : entitiesByDistance.entrySet()) {
                        Log.log(entry);
                    }
                    
                    Map< Integer, Planet> planet = gameMap.getAllPlanets();
                    List <ship> ships = gameMap.getAllShips();
                    for(int current = 0; i < ships.size()){
                        ThrustMove newThrustMove = Navigation.navigateShipToDock(gameMap, ships[current], dockTarget, maxThrust)
                    }
                  /*  if (ship.canDock(planet)) {
                        moveList.add(new DockMove(ship, planet));
                        break;
                    }

                    final ThrustMove newThrustMove = Navigation.navigateShipToDock(gameMap, ship, planet, Constants.MAX_SPEED/2);
                    if (newThrustMove != null) {
                        moveList.add(newThrustMove);
                    }*/

                    break;
                }
            }
            Networking.sendMoves(moveList);
        }
    }
}
