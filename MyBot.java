import hlt.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.Random;
import org.w3c.dom.Entity;

public class MyBot {

    public static void main(final String[] args) {
        final Networking networking = new Networking();
        final GameMap gameMap = networking.initialize("Tamagocchi");
        // We now have 1 full minute to analyse the initial map.
        final String initialMapIntelligence = "width: " + gameMap.getWidth() + "; height: " + gameMap.getHeight()
                + "; players: " + gameMap.getAllPlayers().size() + "; planets: " + gameMap.getAllPlanets().size();
        Log.log(initialMapIntelligence);
        int[] statusAboutAllPlanet;
        int[] statusAboutShipsMove;
        statusAboutAllPlanet = new int[5000];
        statusAboutShipsMove = new int[5000];
        boolean onlyCapturePlanet = false;
        int enemyPlanet = 0;
        final ArrayList<Move> moveList = new ArrayList<>();
        for (final Planet planet : gameMap.getAllPlanets().values()) {
            statusAboutAllPlanet[planet.getId()] = -1;
        }
        for (final Ship ship : gameMap.getMyPlayer().getShips().values()) {
            statusAboutShipsMove[ship.getId()] = 0;
        }
        for (;;) {
            moveList.clear();
            networking.updateMap(gameMap);
            int emptyPlanet = 0;
            for (final Planet planet : gameMap.getAllPlanets().values()) {
                if (!planet.isOwned()) {
                    statusAboutAllPlanet[planet.getId()] = -1;
                    emptyPlanet++;
                }
            }
            int flipFlopArgShip = 0;
            
            for (final Ship ship : gameMap.getMyPlayer().getShips().values()) {
                if (ship.getDockingStatus() != Ship.DockingStatus.Undocked) {
                    continue;
                }

                int ArgShip = 0;
                Random random = new Random();
                int randomArg = random.nextInt(101);
                if (randomArg <= 30) {
                    ArgShip = 1;
                }

                if (ship.getHealth() <= 3) {
                    statusAboutAllPlanet[statusAboutShipsMove[ship.getId()]] = -1;
                    statusAboutShipsMove[ship.getId()] = 0;
                }
                Map<Double, hlt.Entity> nearPlanet = gameMap.nearbyEntitiesByDistance(ship);
                Map<Double, Planet> nearbyEntities = new TreeMap<>();
                for (final Planet planet : gameMap.getAllPlanets().values()) {
                    nearbyEntities.put(ship.getDistanceTo(planet), planet);
                }
                for (Map.Entry<Double, Planet> entry : nearbyEntities.entrySet()) {
                    Planet planet = entry.getValue();
                    if (ArgShip == 1) {
                        if (planet.isOwned() && gameMap.getMyPlayerId() != planet.getOwner()) {
                            int enemy = planet.getDockedShips().get(0);
                            final ThrustMove rushmove = Navigation.navigateShipTowardsTarget(gameMap, ship, gameMap.getShip(planet.getOwner(), enemy).getClosestPoint(ship), Constants.MAX_SPEED, true, Constants.MAX_NAVIGATION_CORRECTIONS, Math.PI/180.0);
                            if (rushmove != null) {
                                moveList.add(rushmove);
                            }
                        }
                        break;
                    }
                    if (gameMap.getMyPlayerId() == planet.getOwner() && !planet.isFull() && ship.canDock(planet)) {
                        if (flipFlopArgShip == 0) {
                            moveList.add(new DockMove(ship, planet));
                            flipFlopArgShip = 1;
                            break;
                        } else {
                            flipFlopArgShip = 0;
                        }
                        
                    }

                    if (planet.isOwned() && gameMap.getMyPlayerId() == planet.getOwner()) {
                        continue;
                    }

                    if (ship.canDock(planet) && !planet.isOwned()) {
                        statusAboutShipsMove[ship.getId()] = planet.getId();
                        statusAboutAllPlanet[planet.getId()] = ship.getId();
                        moveList.add(new DockMove(ship, planet));
                        break;
                    }
                    /*
                     * if (enemyPlanet < 5) { int enemy = planet.getDockedShips().get(0);
                     * moveList.add(Navigation.navigateShipTowardsTarget(gameMap, ship,
                     * ship.getClosestPoint(gameMap.getShip(planet.getOwner(), enemy)), 1, true,
                     * (Constants.MAX_NAVIGATION_CORRECTIONS - 1), Constants.MAX_SPEED)); break; }
                     */

                    if (statusAboutAllPlanet[planet.getId()] > -1 && emptyPlanet >= 2) {
                        continue;
                    }

                    if (emptyPlanet < 2 && planet.isOwned() && gameMap.getMyPlayerId() != planet.getOwner()) {
                        if ((ship.canDock(planet)) && (planet.getOwner() == 0)) {
                            statusAboutShipsMove[ship.getId()] = planet.getId();
                            statusAboutAllPlanet[planet.getId()] = ship.getId();
                            moveList.add(new DockMove(ship, planet));
                            break;
                        }
                        int enemy = planet.getDockedShips().get(0);
                        if (planet.isOwned()) {
                          //  Log.log(enemy);
                            final ThrustMove rushmove = Navigation.navigateShipTowardsTarget(gameMap, ship, gameMap.getShip(planet.getOwner(), enemy).getClosestPoint(ship), Constants.MAX_SPEED, true, Constants.MAX_NAVIGATION_CORRECTIONS, Math.PI/180.0);
                            if (rushmove != null) {
                                moveList.add(rushmove);
                            }
                        }
                        break;
                    }

                    final ThrustMove newThrustMove = Navigation.navigateShipToDock(gameMap, ship, planet,
                            Constants.MAX_SPEED);
                    if (newThrustMove != null) {
                        statusAboutShipsMove[ship.getId()] = planet.getId();
                        statusAboutAllPlanet[planet.getId()] = ship.getId();
                        moveList.add(newThrustMove);
                    }

                    break;
                }
            }
            Networking.sendMoves(moveList);
        }
    }
}
